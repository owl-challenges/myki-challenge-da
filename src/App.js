import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import HomePage from './pages/Home';

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/'>
            <HomePage />
          </Route>
        </Switch>
      </Router>
    );
  }
}
